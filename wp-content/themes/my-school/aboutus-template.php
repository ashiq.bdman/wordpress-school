 <?php
/*
Template Name: About Us
*/

get_header();


$about_us = get_field('about_us');
$image_url = get_field('image_url');
$student_counter = get_field('student_counter');
$teacher_counter = get_field('teacher_counter');
$books_counter = get_field('books_counter');
$parents_counter = get_field('parents_counter');

?>

    
    <!--Banner Wrap Start-->
    <section class="sub_banner_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="sub_banner_hdg">
                        <h3>About US</h3>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ct_breadcrumb">
                        <ul>
                            <li><?php if (function_exists('wptricks_custom_breadcrumbs')){ wptricks_custom_breadcrumbs(); }?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="ct_content_wrap">
        <!--Get Started Wrap Start-->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="get_started_content_wrap ct_blog_detail_des_list">
                            <?php echo $about_us; ?>
                        </div>
                    </div>
                
                    <div class="col-md-6">
                        <div class="get_started_video">
                            <img src="<?php echo $image_url;?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Get Started Wrap End-->


        <section>
            <div class="container">
                <!--Heading Style 1 Wrap Start-->
                <div class="ct_heading_1_wrap">
                    <h3>More About Us</h3>
                    <span><img src="<?php echo get_template_directory_uri(); ?>/images/hdg-01.png" alt=""></span>
                </div>
                <!--Heading Style 1 Wrap End-->

                <!--Faqs and Term List Wrap Start-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="accor_outer_wrap">
                            <div class="ct_accord_list">
                                <div class="accord_hdg accordion-close" id="accord1">
                                    <h6>Ethics</h6>
                                    <span class="fa fa-angle-down"></span>
                                </div>
                                <div class="accord_des" style="display: none;">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>
                                </div>
                            </div>
                            <div class="ct_accord_list">
                                <div class="accord_hdg accordion-close" id="accord2">
                                    <h6>Character Building</h6>
                                    <span class="fa fa-angle-down"></span>
                                </div>
                                <div class="accord_des" style="display: none;">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>
                                </div>
                            </div>
                            <div class="ct_accord_list">
                                <div class="accord_hdg accordion-close" id="accord3">
                                    <h6>Connecting Minds</h6>
                                    <span class="fa fa-angle-down"></span>
                                </div>
                                <div class="accord_des" style="display: none;">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="accor_outer_wrap">
                            <div class="ct_accord_list">
                                <div class="accord_list_1 accordion-close" id="accord_1">
                                    <h6>Creaticity</h6>
                                    <span class="fa fa-angle-down"></span>
                                </div>
                                <div class="accord_des" style="display: none;">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>
                                </div>
                            </div>
                            <div class="ct_accord_list">
                                <div class="accord_list_1 accordion-close" id="accord_2">
                                    <h6>Frame </h6>
                                    <span class="fa fa-angle-down"></span>
                                </div>
                                <div class="accord_des" style="display: none;">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>
                                </div>
                            </div>
                            <div class="ct_accord_list">
                                <div class="accord_list_1 accordion-close" id="accord_3">
                                    <h6>Duke of Edinburgh</h6>
                                    <span class="fa fa-angle-down"></span>
                                </div>
                                <div class="accord_des" style="display: none;">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Faqs and Term List Wrap End-->

            </div>
        </section>

                
        <!--Learn More Wrap Start-->
        <div class="ct_learn_more_bg">
            <div class="container">
                <div class="ct_learn_more">
                    <h4>We provide universal access to the world’s best <span>education.</span></h4>
                    <a href="#">Learn More</a>
                </div>
            </div>
        </div>
        <!--Learn More Wrap End-->
        
    </div>
    <!--Content Wrap End-->

<?php get_footer();?>    