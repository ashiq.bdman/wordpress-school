<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package My_School
 */

get_header();
?>

<!--Banner Wrap Start-->
<section class="sub_banner_wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="sub_banner_hdg">
                    <h3>Course Detail</h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ct_breadcrumb">
                    <ul>
                        <li><?php if (function_exists('wptricks_custom_breadcrumbs')){ wptricks_custom_breadcrumbs(); }?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Banner Wrap End-->

<!--Banner Wrap End-->
<div class="ct_content_wrap">
    <section class="">
        <div class="container">
            <div class="row">

                    <?php 
                    // the query
                    $args = 
                    array(
                      'post_type' => 'alc',
                      'category_name' =>'alc',
                      'order'         =>'ASC'
                     );


                    $the_query = new WP_Query( $args ); ?>

                    <?php if ( $the_query->have_posts() ) : ?>

                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-alc', get_post_type() );

			endwhile;



		else :

			get_template_part( 'template-parts/content-alc', 'none' );

		endif;
		?>

            </div>
        </div>
    </section>

</div>

<?php

get_footer();
