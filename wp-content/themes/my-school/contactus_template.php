 <?php
/*
Template Name: Contact Us
*/

get_header();

$address = get_field('address');
$phone = get_field('phone');
$email = get_field('email');
$map = get_field('map');
?>

    
    <!--Banner Wrap Start-->
    <section class="sub_banner_wrap">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="sub_banner_hdg">
                        <h3>Contact US</h3>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ct_breadcrumb">
                        <ul>
                            <li><?php if (function_exists('wptricks_custom_breadcrumbs')){ wptricks_custom_breadcrumbs(); }?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="ct_content_wrap">
        <!--Get in Touch With Us Wrap Start-->
        <section>
            <div class="container">
                <div class="get_touch_wrap">
                    <h4>GET IN TOUCH WITH US:</h4>
                    <!-- <p>We are home to 1,500 students (aged 12 to 16) and 100 expert faculty and staff community representing over 40 different nations. We are proud of our international and multi-cultural ethos, the way our community collaborates to make a difference. Our world-renowned curriculum is built on the best.</p> -->
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="ct_contact_form">
                            <form>
                                <div class="form_field">
                                    <label class="fa fa-user"></label>
                                    <input class="conatct_plchldr" type="text" placeholder="Your Name">
                                </div>
                                <div class="form_field">
                                    <label class="fa fa-envelope-o"></label>
                                    <input class="conatct_plchldr" type="text" placeholder="Email Address">
                                </div>
                                <div class="form_field">
                                    <label class="fa fa-edit"></label>
                                    <textarea class="conatct_plchldr" placeholder="Write Detail"></textarea>
                                </div>
                                <div class="form_field">
                                    <button>Send Now <i class="fa fa-arrow-right"></i> </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <div class="col-md-5">
                        <div class="bottom_border">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="ct_contact_address">
                                        <h5><i class="fa fa-map-o"></i>Address</h5>
                                        <p><?php echo $address ?></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="ct_contact_address">
                                        <h5><i class="fa fa-envelope-o"></i>Phone</h5>
                                        <ul class="fax_info">
                                            <li><?php echo $phone ?></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="ct_contact_address">
                                        <h5><i class="fa fa-envelope-o"></i>Email</h5>
                                        <ul class="fax_info">
                                            <li><?php echo $email ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!--Get in Touch With Us Wrap End-->

        <!--Map Wrap Start-->
        <iframe src="https://www.google.com/maps/d/embed?mid=1TK_0wU5r65i8lCrouy7uSCDZcQAFoLmE" width="640" height="480"></iframe>
        <!--Map Wrap End-->
        
    </div>
    <!--Content Wrap End-->

<?php get_footer();?>    
