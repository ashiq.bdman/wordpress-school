<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package My_School
 */

?>

	</div><!-- #content -->

    <!--Footer Wrap Start-->
    <footer>

        <!--Footer Col Wrap Start-->
        <div class="ct_footer_bg">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-3 col-sm-6">
                    	<div class="footer_col_1 widget">
                        	<?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-sm-6">
                    	<div class="foo_col_2 widget">
                            <?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
                        	
                        </div>
                    </div>
                    
                     <div class="col-md-3 col-sm-6">
                    	<div class="foo_col_2 widget">
                            <?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
                            
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-sm-6">
                    	<div class="foo_col_4 widget">
                            <?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!--Footer Col Wrap End-->
        
        <!--Footer Copyright Wrap Start-->
        <div class="ct_copyright_bg">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-6">
                    	<div class="copyright_text">
                        	<?php echo get_theme_mod( 'footer-copyright'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<div class="copyright_social_icon">
                        	<ul>
                            	<li><a href="<?php echo get_theme_mod('pinterest-link'); ?>"><i class="fa fa-pinterest"></i></a>
                                </li>
                                <li><a href="<?php echo get_theme_mod( 'linkedn-link'); ?>"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li><a href="<?php echo get_theme_mod( 'twitter-link'); ?>" ><i class="fa fa-twitter"></i></a>
                                </li>
                                <li><a href="<?php echo get_theme_mod( 'facebook-link');?>" ><i class="fa fa-facebook"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer Copyright Wrap End-->
        <div class="back_to_top">
            <a href="#"><i class="fa fa-angle-up"></i></a>
        </div>
    </footer>
    <!--Footer Wrap End-->
        
</div>
<!--Wrapper End-->

    <!--Bootstrap core JavaScript-->
    
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <!--Bx-Slider JavaScript-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
    <!--Dl Menu Script-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/dl-menu/modernizr.custom.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/dl-menu/jquery.dlmenu.js"></script>
    <!--Owl Carousel JavaScript-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
    <!--Time Counter Javascript-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.downCount.js"></script>
    <!--Pretty Photo Javascript-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.prettyPhoto.js"></script>
    <!--Way Points Javascript-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/waypoints-min.js"></script>
    <!--Custom JavaScript-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>

    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.accordion.js"></script>


<?php wp_footer(); ?>

</body>
</html>
