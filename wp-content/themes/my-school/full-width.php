<?php
/*
 * Template Name: Full width Post
 * Template Post Type: post
 */
  
 get_header(); 
?>
<!--Banner Wrap Start-->
<section class="sub_banner_wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="sub_banner_hdg">
                    <h3><?php the_title(); ?></h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ct_breadcrumb">
                    <ul>
                        <li><?php if (function_exists('wptricks_custom_breadcrumbs')){ wptricks_custom_breadcrumbs(); }?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Banner Wrap End-->
<div class="ct_content_wrap">
    <section class="ct_blog_outer_wrap">
        <div class="container">
            <div class="row">

             <?php
                while ( have_posts() ) :the_post();

             ?>
             <!--Blog Detail Wrap Start-->
                <div class="col-md-12">
                    <div class="ct_blog_detail_outer_wrap">
                        <div class="ct_blog_detail_top">
                            <h4><?php the_title(); ?></h4>
                        </div>
                        <div class="ct_blog_detail_des">
                            <figure>
                                <img src="<?php the_post_thumbnail_url( 'medium' );?>"/>
                            </figure>
                            <div class="ct_course_detail_wrap">
                                <h5>Detail Information</h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="ct_blog_detail_des_list">
                                            <p><?php echo wpautop( the_content());?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pre_next">
                            <?php previous_post_link( '%link','Previous' ) ?>

                            
                            <?php next_post_link( '%link','Next' ) ?>

                        </div>

                    </div>
                </div>
                <!--Blog Detail Wrap End-->

            </div>
        </div>
    </section>

</div>

<?php
endwhile; // End of the loop.
?>



<?php
get_footer();
