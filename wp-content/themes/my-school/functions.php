<?php
/**
 * My School functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package My_School
 */

if ( ! function_exists( 'school_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function school_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on My School, use a find and replace
		 * to change 'school' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'school', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

    // Add featured image sizes
    add_image_size( 'featured-large', 1400, 750, true ); // width, height, crop
    add_image_size( 'featured-small', 320, 147, true );

    // Add other useful image sizes for use through Add Media modal
    add_image_size( 'medium-width', 480 );
    add_image_size( 'medium-height', 999, 480 );
    add_image_size( 'large-height', 1400, 750, true );



		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'school' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'school_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'school_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function school_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'school_content_width', 640 );
}
add_action( 'after_setup_theme', 'school_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function school_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'school' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'school' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar-2', 'school' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'school' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );


	// First footer widget area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'First Footer Widget Area', 'tutsplus' ),
        'id' => 'first-footer-widget-area',
        'description' => __( 'The first footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
    ) );
 
    // Second Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Second Footer Widget Area', 'tutsplus' ),
        'id' => 'second-footer-widget-area',
        'description' => __( 'The second footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
    ) );
 
    // Third Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Third Footer Widget Area', 'tutsplus' ),
        'id' => 'third-footer-widget-area',
        'description' => __( 'The third footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
    ) );
 
    // Fourth Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Fourth Footer Widget Area', 'tutsplus' ),
        'id' => 'fourth-footer-widget-area',
        'description' => __( 'The fourth footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
    ) );
}
add_action( 'widgets_init', 'school_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function school_scripts() {

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');

	wp_enqueue_style( 'bxslider', get_template_directory_uri() . '/css/jquery.bxslider.css');

	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css');

	wp_enqueue_style( 'svg-style', get_template_directory_uri() . '/css/svg-style.css');	

	wp_enqueue_style( 'prettyphoto', get_template_directory_uri() . '/css/prettyPhoto.css');
	
	wp_enqueue_style( 'widget', get_template_directory_uri() . '/css/widget.css');

	wp_enqueue_style( 'component', get_template_directory_uri() . '/js/dl-menu/component.css');

	wp_enqueue_style( 'typography', get_template_directory_uri() . '/css/typography.css');

	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css');

	wp_enqueue_style( 'carousel', get_template_directory_uri() . '/css/owl.carousel.css');

	wp_enqueue_style( 'shortcode', get_template_directory_uri() . '/css/shortcodes.css');

	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css');

	wp_enqueue_style( 'color', get_template_directory_uri() . '/css/color.css');

	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css');

// End of enqueue CSS

												
	wp_enqueue_script( 'school-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'school-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'school_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Implement the Custom Post Services feature.
 */
require get_template_directory() . '/inc/custom-function.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );


function namespace_add_custom_types( $query ) {
  if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array(
     'post', 'nav_menu_item', 'course'
		));
	  return $query;
	}
}
add_filter( 'pre_get_posts', 'namespace_add_custom_types' );

// if ( ! function_exists( 'post_pagination' ) ) :
//    function post_pagination() {
//      global $wp_query;
//      $pager = 999999999; // need an unlikely integer
 
//         echo paginate_links( array(
//              'base' => str_replace( $pager, '%#%', esc_url( get_pagenum_link( $pager ) ) ),
//              'format' => '?paged=%#%',
//              'current' => max( 1, get_query_var('paged') ),
//              'total' => $wp_query->max_num_pages
//         ) );
//    }
// endif;


// breadcrumb function
function wptricks_custom_breadcrumbs() {
  $home_breadcrumbs = 0; // put 1 to show breadcrumbs on home page, otherwise leave it as it is
  $separator = '&raquo;'; // separator
  $home = 'Home'; // home text
  $mycurrent = 1; //put 1 to show current post/page title in breadcrumbs, otherwise leave put 0
  global $post;
  $myhome_url = get_bloginfo('url');
  if (is_home() || is_front_page()) {
    if ($home_breadcrumbs == 1) echo '<div id="crumbs"><a href="' . $myhome_url . '">' . $home . '</a></div>';
  } else {
    echo '<div id="crumbs"><a href="' . $myhome_url . '">' . $home . '</a> ' . $separator . ' ';
    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $separator . ' ');
      echo '<span>' . 'Archive :"' . single_cat_title('', false) . '"' . '</span>';
    } elseif ( is_search() ) {
      echo '<span>' . 'Results :"' . get_search_query() . '"' . '</span>';
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $separator . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $separator . ' ';
      echo '<span>' . get_the_time('d') . '</span>';
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $separator . ' ';
      echo '<span>' . get_the_time('F') . '</span>';
    } elseif ( is_year() ) {
      echo '<span>' . get_the_time('Y') . '</span>';
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $myhome_url . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
        if ($mycurrent == 1) echo ' ' . $separator . ' ' . '<span>' . get_the_title() . '</span>';
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $separator . ' ');
        if ($mycurrent == 0) $cats = preg_replace("#^(.+)\s$separator\s$#", "$1", $cats);
        echo $cats;
        if ($mycurrent == 1) echo '<span>' . get_the_title() . '</span>';
      }
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo '<span>' . $post_type->labels->singular_name . '</span>';
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $separator . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
      if ($mycurrent == 1) echo ' ' . $separator . ' ' . '<span>' . get_the_title() . '</span>';
    } elseif ( is_page() && !$post->post_parent ) {
      if ($mycurrent == 1) echo '<span>' . get_the_title() . '</span>';
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $separator . ' ';
      }
      if ($mycurrent == 1) echo ' ' . $separator . ' ' . '<span>' . get_the_title() . '</span>';
    } elseif ( is_tag() ) {
      echo '<span>' . 'Posts tagged "' . single_tag_title('', false) . '"' . '</span>';
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo '<span>' . 'Articles By ' . $userdata->display_name . '</span>';
    } elseif ( is_404() ) {
      echo '<span>' . '404' . '</span>';
    }
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
    echo '</div>';
  }
}


/*********************************/
/* Change Search Button Text
/**************************************/
 
// Add to your child-theme functions.php
add_filter('get_search_form', 'my_search_form_text');
 
function my_search_form_text($text) {
     $text = str_replace('value="Search"', 'value="GO"', $text); //set as value the text you want
     return $text;
}