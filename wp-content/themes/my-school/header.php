<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package My_School
 */
$header_image         = get_field('header_image');

?>
<!doctype html>
<html <?php language_attributes(); ?>>
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Edu Learn - Education Theme</title>

    <style type="text/css">
        .sub_banner_wrap{
            background-image:url(<?php echo $header_image?>);
            background-repeat:no-repeat;
            background-size:cover;
            background-position:center;
            z-index:10;
        }
    </style>
    
    <?php wp_head(); ?>
 
  </head>

<body <?php body_class(); ?>>

<!--Wrapper Start-->  
<div class="ct_wrapper">
	
    <!--Header Wrap Start-->
    <header>
    	<!--Top Strip Wrap Start-->
        <div class="top_strip">
        	<div class="container">
                <div class="top_location_wrap">

                    <?php if( get_theme_mod( 'header_text_block') != "" ); ?>

                    <p><i class="fa fa-map-marker"></i><?php echo get_theme_mod( 'header_text_block'); ?></p>
                </div>
                <div class="top_ui_element">
                    <ul>
                        <li><i class="fa fa-envelope"></i><a href="#"><?php echo get_theme_mod( 'header_email_block'); ?></a></li>
                        <li><i class="fa fa-phone"></i><a href="#"><?php echo get_theme_mod( 'header_phone_block'); ?></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--Top Strip Wrap End-->
        
        <!--Navigation Wrap Start-->
        <div class="logo_nav_outer_wrap">
        	<div class="container">
                <div class="logo_wrap">
                <?php if( has_custom_logo() ) { 
                  the_custom_logo(); 
                    } else { ?>
                    <h1 class="navbar-brand mb-0"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                <?php } ?>
                </div>
                <div class="top_search_wrap">
                	<i class="fa fa-search search-fld"></i>
                    <div class="search-wrapper-area">
                        <form class="search-area">
                            <?php echo get_search_form();?>
                        </form>
                    </div>
                </div>
                <nav class="main_navigation">
                        <?php
                                wp_nav_menu( array(
                                    'theme_location' => 'menu-1',
                                    'menu_id'        => 'primary-menu',
                                    'menu_class'       => 'navigation-menu'
                                ) );
                            ?>
                </nav>
                <!--DL Menu Start-->
                <!-- <div id="kode-responsive-navigation" class="dl-menuwrapper">
                    <button class="dl-trigger">Open Menu</button>
                    <ul class="dl-menu">
                        <li class="active"><a href="index-2.html">Home</a></li>
                        <li><a href="about-us.html">About Us</a></li>
                        <li class="menu-item kode-parent-menu"><a href="#">courses</a>
                            <ul class="dl-submenu">
                                <li><a href="course-listing.html">Course Listing</a></li>
                                <li><a href="course-detail.html">Course Detail</a></li>
                            </ul>
                        </li>
                        <li class="menu-item kode-parent-menu"><a href="our-teacher.html">Teachers</a></li>
                        <li class="menu-item kode-parent-menu"><a href="#">Blog</a>
                            <ul class="dl-submenu">
                                <li><a href="blog-simple.html">Blog Simple</a></li>
                                <li><a href="blog-grid.html">Blog Grid</a></li>
                                <li><a href="blog-detail.html">Blog Detail</a></li>
                            </ul>
                        </li>
                        <li class="menu-item kode-parent-menu"><a href="#">Pages</a>
                            <ul class="dl-submenu">
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="404-page.html">404 Page</a></li>
                                <li><a href="comming-soon.html">Comming Soon</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us.html">Contact Us</a></li>
                    </ul>
                </div> -->
                <!--DL Menu END-->
            </div>
        </div>
        <!--Navigation Wrap End-->
    </header>
    <!--Header Wrap End-->