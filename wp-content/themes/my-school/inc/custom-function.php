<?php
/**
 * My School functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package My_School
 */
//customize the header section

function school_customize_header($wp_customize){

	// Create custom panel.
	$wp_customize->add_panel( 'text_blocks', array(
		'priority'       => 50,
		'theme_supports' => '',
		'title'          => __( 'Header Blocks', 'my_school' ),
		'description'    => __( 'Set editable text for certain content.', 'my_school' ),
	) );
	// Add Header Text
	// Add section.
	$wp_customize->add_section( 'custom_header_text' , array(
		'title'    => __('Change Header Setting','my_school'),
		'panel'    => 'text_blocks',
		'priority' => 5
	) );
	// Add setting
	$wp_customize->add_setting( 'header_text_block', array(
		 'default'           => __( '33-New West Side, Barcelona NY US', 'my_school' ),
		 'sanitize_callback' => 'sanitize_text'
	) );

	$wp_customize->add_setting( 'header_email_block', array(
		 'default'           => __( 'info@educare.com', 'my_school' ),
		 'sanitize_callback' => 'sanitize_text'
	) );

	$wp_customize->add_setting( 'header_phone_block', array(
		 'default'           => __( '123 - 555 - 6239', 'my_school' ),
		 'sanitize_callback' => 'sanitize_text'
	) );

	// Add control
	$wp_customize->add_control( new WP_Customize_Control(
	    $wp_customize,
		'custom_footer_text',
		    array(
		        'label'    => __( 'Address', 'my_school' ),
		        'section'  => 'custom_header_text',
		        'settings' => 'header_text_block',
		        'type'     => 'text'
		    )
	    )
	);

	$wp_customize->add_control( new WP_Customize_Control(
	    $wp_customize,
		'header_email_block',
		    array(
		        'label'    => __( 'Email', 'my_school' ),
		        'section'  => 'custom_header_text',
		        'settings' => 'header_email_block',
		        'type'     => 'text'
		    )
	    )
	);	

	$wp_customize->add_control( new WP_Customize_Control(
	    $wp_customize,
		'header_phone_block',
		    array(
		        'label'    => __( 'Phone', 'my_school' ),
		        'section'  => 'custom_header_text',
		        'settings' => 'header_phone_block',
		        'type'     => 'text'
		    )
	    )
	);		
 	// Sanitize text
	function sanitize_text( $text ) {
	    return sanitize_text_field( $text );
	}

}

add_action('customize_register', 'school_customize_header');



function myschool_header_text() {
	if( get_theme_mod( 'header_text_block') != "" ) {
		echo get_theme_mod( 'header_text_block');
	}
	elseif( get_theme_mod( 'header_email_block') != "" ) {
		echo get_theme_mod( 'header_email_block');
	}
	elseif( get_theme_mod( 'header_phone_block') != "" ) {
		echo get_theme_mod( 'header_phone_block');
	}
	else{
		echo 'Default Text'; // Add you default footer text here
	}
}
add_filter('myschool_header', 'myschool_header_text');




// Add slider options to admin appearance in customize screen

function my_school_banner($wp_customize){

	$wp_customize->add_section('banner-options', array(
		'title' => 'Banner Options'		
	));


	$wp_customize->add_setting('banner-heading', array(
		'default' => 'Example Headline Text'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_heading_control', array(

		'label' => 'Banner Heading 1',
		'section' => 'banner-options',
		'settings' => 'banner-heading'
	)));


	$wp_customize->add_setting('banner-text', array(
		'default' => 'Example paragraph Text'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_text_control', array(

		'label' => 'Banner Secondary Text 1',
		'section' => 'banner-options',
		'settings' => 'banner-text',
		'type' => 'textarea'
	)));


	$wp_customize->add_setting('banner-image');

	$wp_customize->add_control(new WP_Customize_cropped_Image_Control($wp_customize, 'my_school_image_control', array(

		'label' => 'Banner Image 1',
		'section' => 'banner-options',
		'settings' => 'banner-image',
		'width' => '1349',
		'height' => '550'
	)));	


	// Banner settings 2


	$wp_customize->add_setting('banner-heading-2', array(
		'default' => 'Example Headline Text 2'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_heading_control_2', array(

		'label' => 'Banner Heading 2',
		'section' => 'banner-options',
		'settings' => 'banner-heading-2'
	)));

	$wp_customize->add_setting('banner-text-2', array(
		'default' => 'Example paragraph Text 2'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_text_control_2', array(

		'label' => 'Banner Secondary Text 2',
		'section' => 'banner-options',
		'settings' => 'banner-text-2',
		'type' => 'textarea'
	)));


	$wp_customize->add_setting('banner-image-2');

	$wp_customize->add_control(new WP_Customize_cropped_Image_Control($wp_customize, 'my_school_image_control_2', array(

		'label' => 'Banner Image 2',
		'section' => 'banner-options',
		'settings' => 'banner-image-2',
		'width' => '1349',
		'height' => '550'
	)));		
	
	// Banner settings 3

	$wp_customize->add_setting('banner-heading-3', array(
		'default' => 'Example Headline Text 3'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_heading_control_3', array(

		'label' => 'Banner Heading 3',
		'section' => 'banner-options',
		'settings' => 'banner-heading-3'
	)));

	$wp_customize->add_setting('banner-text-3', array(
		'default' => 'Example paragraph Text 3'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_text_control_3', array(

		'label' => 'Banner Secondary Text 3',
		'section' => 'banner-options',
		'settings' => 'banner-text-3',
		'type' => 'textarea'
	)));


	$wp_customize->add_setting('banner-image-3');

	$wp_customize->add_control(new WP_Customize_cropped_Image_Control($wp_customize, 'my_school_image_control_3', array(

		'label' => 'Banner Image 3',
		'section' => 'banner-options',
		'settings' => 'banner-image-3',
		'width' => '1349',
		'height' => '550'
	)));		


	// Banner settings 4

	$wp_customize->add_setting('banner-heading-4', array(
		'default' => 'Example Headline Text 4'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_heading_control_4', array(

		'label' => 'Banner Heading 4',
		'section' => 'banner-options',
		'settings' => 'banner-heading-4'
	)));

	$wp_customize->add_setting('banner-text-4', array(
		'default' => 'Example paragraph Text 4'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_text_control_4', array(

		'label' => 'Banner Secondary Text 4',
		'section' => 'banner-options',
		'settings' => 'banner-text-4',
		'type' => 'textarea'
	)));


	$wp_customize->add_setting('banner-image-4');

	$wp_customize->add_control(new WP_Customize_cropped_Image_Control($wp_customize, 'my_school_image_control_4', array(

		'label' => 'Banner Image 4',
		'section' => 'banner-options',
		'settings' => 'banner-image-4',
		'width' => '1349',
		'height' => '550'
	)));		

	// Banner settings 5

	$wp_customize->add_setting('banner-heading-5', array(
		'default' => 'Example Headline Text 5'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_heading_control_5', array(

		'label' => 'Banner Heading 5',
		'section' => 'banner-options',
		'settings' => 'banner-heading-5'
	)));

	$wp_customize->add_setting('banner-text-5', array(
		'default' => 'Example paragraph Text 5'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_text_control_5', array(

		'label' => 'Banner Secondary Text 5',
		'section' => 'banner-options',
		'settings' => 'banner-text-5',
		'type' => 'textarea'
	)));


	$wp_customize->add_setting('banner-image-5');

	$wp_customize->add_control(new WP_Customize_cropped_Image_Control($wp_customize, 'my_school_image_control_5', array(

		'label' => 'Banner Image 5',
		'section' => 'banner-options',
		'settings' => 'banner-image-5',
		'width' => '1349',
		'height' => '550'
	)));		

	// Banner settings 6

	$wp_customize->add_setting('banner-heading-6', array(
		'default' => 'Example Headline Text 6'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_heading_control_6', array(

		'label' => 'Banner Heading 6',
		'section' => 'banner-options',
		'settings' => 'banner-heading-6'
	)));

	$wp_customize->add_setting('banner-text-6', array(
		'default' => 'Example paragraph Text 6'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_text_control_6', array(

		'label' => 'Banner Secondary Text 6',
		'section' => 'banner-options',
		'settings' => 'banner-text-6',
		'type' => 'textarea'
	)));


	$wp_customize->add_setting('banner-image-6');

	$wp_customize->add_control(new WP_Customize_cropped_Image_Control($wp_customize, 'my_school_image_control_6', array(

		'label' => 'Banner Image 6',
		'section' => 'banner-options',
		'settings' => 'banner-image-6',
		'width' => '1349',
		'height' => '550'
	)));	


	// Banner settings 7

	$wp_customize->add_setting('banner-heading-7', array(
		'default' => 'Example Headline Text 7'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_heading_control_7', array(

		'label' => 'Banner Heading 7',
		'section' => 'banner-options',
		'settings' => 'banner-heading-7'
	)));

	$wp_customize->add_setting('banner-text-7', array(
		'default' => 'Example paragraph Text 7'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_text_control_7', array(

		'label' => 'Banner Secondary Text 7',
		'section' => 'banner-options',
		'settings' => 'banner-text-7',
		'type' => 'textarea'
	)));


	$wp_customize->add_setting('banner-image-7');

	$wp_customize->add_control(new WP_Customize_cropped_Image_Control($wp_customize, 'my_school_image_control_7', array(

		'label' => 'Banner Image 7',
		'section' => 'banner-options',
		'settings' => 'banner-image-7',
		'width' => '1349',
		'height' => '550'
	)));		

}
add_action('customize_register', 'my_school_banner');


// Add footer option to theme customize section


function my_school_footer($wp_customize){

	$wp_customize->add_section('footer-options', array(
		'title' => 'Footer Options'		
	));


	$wp_customize->add_setting('footer-copyright', array(
		'default' => 'Example Copyright Text'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_copyright_control', array(

		'label' => 'Copyright Text',
		'section' => 'footer-options',
		'settings' => 'footer-copyright',		
		'type' => 'textarea'
	)));

// add social media link

	$wp_customize->add_setting('facebook-link', array(
		'default' => '#'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_facebook_link', array(

		'label' => 'Facebeek Link',
		'section' => 'footer-options',
		'settings' => 'facebook-link'
	)));


	$wp_customize->add_setting('twitter-link', array(
		'default' => '#'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_twitter_link', array(

		'label' => 'Twitter Link',
		'section' => 'footer-options',
		'settings' => 'twitter-link'
	)));


	$wp_customize->add_setting('linkedn-link', array(
		'default' => '#'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_linkedn_link', array(

		'label' => 'Linkedn Link',
		'section' => 'footer-options',
		'settings' => 'linkedn-link'
	)));

	$wp_customize->add_setting('pinterest-link', array(
		'default' => '#'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_pinterest_link', array(

		'label' => 'Pinterest Link',
		'section' => 'footer-options',
		'settings' => 'pinterest-link'
	)));

}
add_action('customize_register', 'my_school_footer');



// Register Custom Taxonomy
function menu_item_courses() {

	$labels = array(
		'name'                       => _x( 'Courses', 'Post Type General Name', 'my-school' ),
		'singular_name'              => _x( 'course', 'Post Type Singular Name', 'my-school' ),
		'menu_name'                  => __( 'Courses', 'my-school' ),
		'name_admin_bar'			=> __( 'course', 'my-school' ),
		'all_items'                  => __( 'All course Item', 'my-school' ),
		'parent_item'                => __( 'Parent Item', 'my-school' ),
		'parent_item_colon'          => __( 'Parent Item:', 'my-school' ),
		'new_item_name'              => __( 'New course', 'my-school' ),
		'add_new_item'               => __( 'Add New course', 'my-school' ),
		'edit_item'                  => __( 'Edit course', 'my-school' ),
		'update_item'                => __( 'Update course', 'my-school' ),
		'view_item'                  => __( 'View course', 'my-school' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'my-school' ),
		'add_or_remove_items'        => __( 'Add or remove course', 'my-school' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'my-school' ),
		'popular_items'              => __( 'Popular Courses', 'my-school' ),
		'search_items'               => __( 'Search course', 'my-school' ),
		'not_found'                  => __( 'Not Found', 'my-school' ),
		'no_terms'                   => __( 'No items', 'my-school' ),
		'items_list'                 => __( 'Items list', 'my-school' ),
		'items_list_navigation'      => __( 'Items list navigation', 'my-school' ),
	);
	$args = array(
		'label'                 => __( 'course', 'my-school' ),
		'description'           => __( 'Post Type Description', 'my-school' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'post-formats', 'excerpt' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'				=> 'dashicons-welcome-write-blog',
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'course', $args );

}
add_action( 'init', 'menu_item_courses', 0 );


// Custom post for testimonial

function custom_post_testimonial() {

	$labels = array(
		'name'                       => _x( 'Testimonial', 'Post Type General Name', 'my-school' ),
		'singular_name'              => _x( 'testimonial', 'Post Type Singular Name', 'my-school' ),
		'menu_name'                  => __( 'Testimonial', 'my-school' ),
		'name_admin_bar'			=> __( 'testimonial', 'my-school' ),
		'all_items'                  => __( 'All testimonial Item', 'my-school' ),
		'parent_item'                => __( 'Parent Item', 'my-school' ),
		'parent_item_colon'          => __( 'Parent Item:', 'my-school' ),
		'new_item_name'              => __( 'New testimonial', 'my-school' ),
		'add_new_item'               => __( 'Add New testimonial', 'my-school' ),
		'edit_item'                  => __( 'Edit testimonial', 'my-school' ),
		'update_item'                => __( 'Update testimonial', 'my-school' ),
		'view_item'                  => __( 'View testimonial', 'my-school' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'my-school' ),
		'add_or_remove_items'        => __( 'Add or remove testimonial', 'my-school' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'my-school' ),
		'popular_items'              => __( 'Popular Testimonial', 'my-school' ),
		'search_items'               => __( 'Search testimonial', 'my-school' ),
		'not_found'                  => __( 'Not Found', 'my-school' ),
		'no_terms'                   => __( 'No items', 'my-school' ),
		'items_list'                 => __( 'Items list', 'my-school' ),
		'items_list_navigation'      => __( 'Items list navigation', 'my-school' ),
	);
	$args = array(
		'label'                 => __( 'testimonial', 'my-school' ),
		'description'           => __( 'Post Type Description', 'my-school' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'post-formats', 'excerpt' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'   			=> 'dashicons-format-quote',		
		'menu_position'         => 6,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'testimonial', $args );

}
add_action( 'init', 'custom_post_testimonial', 0 );


// Custom post for teacher

function custom_post_teacher() {

	$labels = array(
		'name'                       => _x( 'Teacher', 'Post Type General Name', 'my-school' ),
		'singular_name'              => _x( 'teacher', 'Post Type Singular Name', 'my-school' ),
		'menu_name'                  => __( 'Teacher', 'my-school' ),
		'name_admin_bar'			 => __( 'teacher', 'my-school' ),
		'all_items'                  => __( 'All teacher Item', 'my-school' ),
		'parent_item'                => __( 'Parent Item', 'my-school' ),
		'parent_item_colon'          => __( 'Parent Item:', 'my-school' ),
		'new_item_name'              => __( 'New teacher', 'my-school' ),
		'add_new_item'               => __( 'Add New teacher', 'my-school' ),
		'edit_item'                  => __( 'Edit teacher', 'my-school' ),
		'update_item'                => __( 'Update teacher', 'my-school' ),
		'view_item'                  => __( 'View teacher', 'my-school' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'my-school' ),
		'add_or_remove_items'        => __( 'Add or remove teacher', 'my-school' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'my-school' ),
		'popular_items'              => __( 'Popular teacher', 'my-school' ),
		'search_items'               => __( 'Search teacher', 'my-school' ),
		'not_found'                  => __( 'Not Found', 'my-school' ),
		'no_terms'                   => __( 'No items', 'my-school' ),
		'items_list'                 => __( 'Items list', 'my-school' ),
		'items_list_navigation'      => __( 'Items list navigation', 'my-school' ),
	);
	$args = array(
		'label'                 => __( 'Teacher', 'my-school' ),
		'description'           => __( 'Post Type Description', 'my-school' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'post-formats', 'excerpt' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'   			=> 'dashicons-welcome-learn-more',		
		'menu_position'         => 7,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'teacher', $args );

}
add_action( 'init', 'custom_post_teacher', 0 );



// Custom post for Events & News

function custom_post_events_news() {

	$labels = array(
		'name'                       => _x( 'Events', 'Post Type General Name', 'my-school' ),
		'singular_name'              => _x( 'events & news', 'Post Type Singular Name', 'my-school' ),
		'menu_name'                  => __( 'Events & news', 'my-school' ),
		'name_admin_bar'			 => __( 'events & news', 'my-school' ),
		'all_items'                  => __( 'All events & news Item', 'my-school' ),
		'parent_item'                => __( 'Parent Item', 'my-school' ),
		'parent_item_colon'          => __( 'Parent Item:', 'my-school' ),
		'new_item_name'              => __( 'New events & news', 'my-school' ),
		'add_new_item'               => __( 'Add New events & news', 'my-school' ),
		'edit_item'                  => __( 'Edit events & news', 'my-school' ),
		'update_item'                => __( 'Update events & news', 'my-school' ),
		'view_item'                  => __( 'View events & news', 'my-school' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'my-school' ),
		'add_or_remove_items'        => __( 'Add or remove events & news', 'my-school' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'my-school' ),
		'popular_items'              => __( 'Popular events & news', 'my-school' ),
		'search_items'               => __( 'Search events & news', 'my-school' ),
		'not_found'                  => __( 'Not Found', 'my-school' ),
		'no_terms'                   => __( 'No items', 'my-school' ),
		'items_list'                 => __( 'Items list', 'my-school' ),
		'items_list_navigation'      => __( 'Items list navigation', 'my-school' ),
	);
	$args = array(
		'label'                 => __( 'events & news', 'my-school' ),
		'description'           => __( 'Post Type Description', 'my-school' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'post-formats', 'excerpt' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'   			=> 'dashicons-feedback',		
		'menu_position'         => 8,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'events & news', $args );

}
add_action( 'init', 'custom_post_events_news', 0 );



// Custom post for ALC

function custom_post_alc() {

	$labels = array(
		'name'                       => _x( 'ALC', 'Post Type General Name', 'my-school' ),
		'singular_name'              => _x( 'alc', 'Post Type Singular Name', 'my-school' ),
		'menu_name'                  => __( 'ALC', 'my-school' ),
		'name_admin_bar'			 => __( 'alc', 'my-school' ),
		'all_items'                  => __( 'All alc Item', 'my-school' ),
		'parent_item'                => __( 'Parent Item', 'my-school' ),
		'parent_item_colon'          => __( 'Parent Item:', 'my-school' ),
		'new_item_name'              => __( 'New alc', 'my-school' ),
		'add_new_item'               => __( 'Add New alc', 'my-school' ),
		'edit_item'                  => __( 'Edit alc', 'my-school' ),
		'update_item'                => __( 'Update alc', 'my-school' ),
		'view_item'                  => __( 'View alc', 'my-school' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'my-school' ),
		'add_or_remove_items'        => __( 'Add or remove alc', 'my-school' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'my-school' ),
		'popular_items'              => __( 'Popular alc', 'my-school' ),
		'search_items'               => __( 'Search alc', 'my-school' ),
		'not_found'                  => __( 'Not Found', 'my-school' ),
		'no_terms'                   => __( 'No items', 'my-school' ),
		'items_list'                 => __( 'Items list', 'my-school' ),
		'items_list_navigation'      => __( 'Items list navigation', 'my-school' ),
	);
	$args = array(
		'label'                 => __( 'alc', 'my-school' ),
		'description'           => __( 'Post Type Description', 'my-school' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'post-formats', 'excerpt' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'   			=> 'dashicons-feedback',		
		'menu_position'         => 8,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'alc', $args );

}
add_action( 'init', 'custom_post_alc', 0 );


// Register Custom Taxonomy Publications
function menu_item_publication() {

	$labels = array(
		'name'                       => _x( 'Publications', 'Post Type General Name', 'my-school' ),
		'singular_name'              => _x( 'publication', 'Post Type Singular Name', 'my-school' ),
		'menu_name'                  => __( 'Publications', 'my-school' ),
		'name_admin_bar'			=> __( 'publication', 'my-school' ),
		'all_items'                  => __( 'All publications', 'my-school' ),
		'parent_item'                => __( 'Parent Item', 'my-school' ),
		'parent_item_colon'          => __( 'Parent Item:', 'my-school' ),
		'new_item_name'              => __( 'New publication', 'my-school' ),
		'add_new_item'               => __( 'Add New publication', 'my-school' ),
		'edit_item'                  => __( 'Edit publication', 'my-school' ),
		'update_item'                => __( 'Update publication', 'my-school' ),
		'view_item'                  => __( 'View publication', 'my-school' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'my-school' ),
		'add_or_remove_items'        => __( 'Add or remove publication', 'my-school' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'my-school' ),
		'popular_items'              => __( 'Popular Publications', 'my-school' ),
		'search_items'               => __( 'Search publication', 'my-school' ),
		'not_found'                  => __( 'Not Found', 'my-school' ),
		'no_terms'                   => __( 'No items', 'my-school' ),
		'items_list'                 => __( 'Items list', 'my-school' ),
		'items_list_navigation'      => __( 'Items list navigation', 'my-school' ),
	);
	$args = array(
		'label'                 => __( 'publication', 'my-school' ),
		'description'           => __( 'Post Type Description', 'my-school' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'post-formats', 'excerpt' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'				=> 'dashicons-welcome-write-blog',
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'publication', $args );

}
add_action( 'init', 'menu_item_publication', 0 );


// Add box options

function my_school_box($wp_customize){

	$wp_customize->add_section('box-options', array(
		'title' => 'Box Options'		
	));


	$wp_customize->add_setting('box-url', array(
		'default' => 'http://google.com'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_heading_control', array(

		'label' => 'Box URL 1',
		'section' => 'box-options',
		'settings' => 'box-url'
	)));


	$wp_customize->add_setting('box-image');

	$wp_customize->add_control(new WP_Customize_cropped_Image_Control($wp_customize, 'my_school_image_control', array(

		'label' => 'Box Image 1',
		'section' => 'box-options',
		'settings' => 'box-image',
		'width' => '400',
		'height' => '250'
	)));		

	$wp_customize->add_setting('box-url-2', array(
		'default' => 'http://google.com'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_heading_control_2', array(

		'label' => 'Box URL 2',
		'section' => 'box-options',
		'settings' => 'box-url-2'
	)));


	$wp_customize->add_setting('box-image-2');

	$wp_customize->add_control(new WP_Customize_cropped_Image_Control($wp_customize, 'my_school_image_control_2', array(

		'label' => 'Box Image-2',
		'section' => 'box-options',
		'settings' => 'box-image-2',
		'width' => '400',
		'height' => '250'
	)));


	$wp_customize->add_setting('box-url-3', array(
		'default' => 'http://google.com'

	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'my_school_heading_control_3', array(

		'label' => 'Box URL 3',
		'section' => 'box-options',
		'settings' => 'box-url-3'
	)));


	$wp_customize->add_setting('box-image-3');

	$wp_customize->add_control(new WP_Customize_cropped_Image_Control($wp_customize, 'my_school_image_control_3', array(

		'label' => 'Box Image-3',
		'section' => 'box-options',
		'settings' => 'box-image-3',
		'width' => '400',
		'height' => '250'
	)));			

}
add_action('customize_register', 'my_school_box');



function member_view_only( $atts, $content = null ) {

	if ( ( is_user_logged_in() && !is_null( $content ) ) ){

		return do_shortcode($content);
	}

	return '<p>Please sign in to view content</p>';

}

add_shortcode('members','member_view_only');

