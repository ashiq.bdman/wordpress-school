        <!--ALC Wrap Start-->
        <section style="padding-top: 0">
            <div class="container">
                <!--Heading Style 1 Wrap Start-->
                <!-- <div class="ct_heading_1_wrap ct_white_hdg">
                </div> -->
                <!--Heading Style 1 Wrap End-->
                
                <!--ALC Wrap Start-->
                <div class="courses_subject_carousel owl-carousel">
                    <?php 
                    // the query
                    $args = 
                    array(
                      'post_type' => 'alc',
                      'category_name' =>'alc',
                      'order'         =>'ASC'
                     );


                    $the_query = new WP_Query( $args ); ?>

                    <?php if ( $the_query->have_posts() ) : ?>

                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 

                    <div class="item">
                        <div class="course_subject_wrap">
                            <a href="<?php the_permalink();?>"><img src="<?php the_post_thumbnail_url( 'small' );?>"/></a>
                        </div>
                    </div>

                    <?php endwhile; ?>
                    <!-- end of the loop -->

                    <!-- pagination here -->

                    <?php wp_reset_postdata(); ?>

                  <?php endif; ?>
                </div>
                <!--Courses Subject List Wrap End-->
            </div>
        </section>
        <!-- End of ALC -->