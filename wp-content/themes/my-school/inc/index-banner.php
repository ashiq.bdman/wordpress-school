
    <div class="banner_outer_wrap">
    	<ul class="main_slider">

            <?php

            $args = array(
                'post_type' => 'banner',
                'catagory_name' => 'banner',
                'order'    => 'DESC',
                'posts_per_page'=> 4

            );

            $query = new WP_Query($args);  ?>

            <?php if ($query->have_posts()) : ?>

            <?php while($query->have_posts()) : $query->the_post() ?>




        	<li>
            	<img src="<?php the_post_thumbnail_url( 'featured-small' );?>" alt="banner-image">
                <div class="ct_banner_caption">
                	<!-- <h4 class="fadeInDown">WELCOME TO <span>EDU LEARN</span></h4> -->
                    <span class="fadeInDown"><?php the_title() ?></span>
                    <h2 class="fadeInDown"><?php the_content() ?></h2>
                   <!--  <p class="fadeInDown">HELLO, ARE YOU READY TO START RIGHT NOW ?</p> -->
                    <a class="active fadeInDown" href="#">FIND COURSES</a>
                    <a class="fadeInDown" href="#">DISCOVER MORE</a>
                </div>
            </li>

            <?php endwhile; ?>
            <!-- end of the loop -->

            <!-- pagination here -->

            <?php wp_reset_postdata(); ?>

          <?php endif; ?>                 

        </ul>
    </div>
