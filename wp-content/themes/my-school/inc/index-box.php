<section class="info-boxes">
  <div class="container">
    <div class="row">

      <div class="col-sm-4">
          <a href="<?php echo get_theme_mod('box-url');?>" style="background: url(<?php echo wp_get_attachment_url(get_theme_mod('box-image')); ?>);" class="info-box cyan ">
        <div class="info-box-content">
          <h3 class="text-uppercase">PIS SCHOOL</h3><br>
          <i class="fa fa-university fa-4x" aria-hidden="tru<br>e"></i>
        </div>
        </a>
      </div>

      <div class="col-sm-4">
          <a href="<?php echo get_theme_mod('box-url-2');?>" target="_blank" style="background: url(<?php echo wp_get_attachment_url(get_theme_mod('box-image-2')); ?>);" class="info-box orange ">
        <div class="info-box-content">
          <h3 class="text-uppercase">PIS COLLEGE</h3><br>
          <i class="fa fa-cogs fa-4x" aria-hidden="true"></i>
        </div>
        </a>
      </div>

      <div class="col-sm-4">
          <a href="<?php echo get_theme_mod('box-url-3');?>" style="background: url(<?php echo wp_get_attachment_url(get_theme_mod('box-image-3')); ?>);" class="info-box red ">
          <div class="info-box-content">
            <h3 class="text-uppercase">ALC</h3><br>
            <i class="fa fa-graduation-cap fa-4x" aria-hidden="true"></i>
          </div>
        </a>
      </div>

    </div>
  </div>
</section> 

