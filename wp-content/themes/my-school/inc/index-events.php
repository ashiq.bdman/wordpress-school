<section class="event_bg">
	<div class="container">
    	<!--Heading Style 1 Wrap Start-->
        <div class="ct_heading_1_wrap">
        	<h3>Our Events & News</h3>
            <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec <br/>tetuer adipis elit, aliquam eget nibh etlibura.</p>
            <span><img src="<?php echo get_template_directory_uri(); ?>/images/hdg-01.png" alt=""></span>
        </div>
        <!--Heading Style 1 Wrap End-->
        
        <!--Event List Wrap Start-->
        <div class="row">

            <?php 
            // the query
            $args = 
            array(
              'post_type' => 'events & news',
              'category_name' =>'event & news',
              'order'         =>'DESC',
              'posts_per_page'=> 4
             );


            $the_query = new WP_Query( $args ); ?>

            <?php if ( $the_query->have_posts() ) : ?>

            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 
        	<div class="col-md-6">
            	<div class="ct_main_event_wrap">
                    <h4><?php the_title(); ?></h4>
                    <h5>Start Date is : <span>20 june 2016</span></h5>
                    <ul class="event_location_list">
                        <li><span><a href="<?php the_permalink();?>"  style="color:#fff">Read More <i class="fa fa-arrow-right"></i></a></span></li>
                    </ul>
                </div>
            </div>

            <?php endwhile; ?>
            <!-- end of the loop -->

            <!-- pagination here -->

            <?php wp_reset_postdata(); ?>

          <?php endif; ?>                 
        </div>
        <!--Event List Wrap End-->
        
    </div>
</section>