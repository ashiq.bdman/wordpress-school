        <section class="ct_blog_simple_bg">
            <div class="container">
                <!--Heading Style 1 Wrap Start-->
                <div class="ct_heading_1_wrap">
                    <h3>Our News</h3>
                    <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec <br/>tetuer adipis elit, aliquam eget nibh etlibura.</p>
                    <span><img src="<?php echo get_template_directory_uri(); ?>/images/hdg-01.png" alt=""></span>
                </div>
                <!--Heading Style 1 Wrap End-->
                
                <!--Latest News Wrap Start-->
                <div class="row">
                    <div class="col-md-4">
                        <div class="ct_news_wrap">
                            <span>2015-10-08</span>
                            <h5><a href="#">Those Other College Expenses You Aren’t Thinking</a></h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/extra-images/news-img-01.jpg" alt="">
                                <label>John Snow</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ct_news_wrap">
                            <span>2015-10-08</span>
                            <h5><a href="#">Those Other College Expenses You Aren’t Thinking</a></h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/extra-images/news-img-01.jpg" alt="">
                                <label>John Snow</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ct_news_wrap">
                            <span>2015-10-08</span>
                            <h5><a href="#">Those Other College Expenses You Aren’t Thinking</a></h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/extra-images/news-img-01.jpg" alt="">
                                <label>John Snow</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Latest News Wrap End-->
            </div>
        </section>