<section class="ct_courses_subject_bg">
    <div class="container">
        <!--Heading Style 1 Wrap Start-->
        <div class="ct_heading_1_wrap ct_white_hdg">
            <h3>PIS Offering</h3>
            <span><img src="<?php echo get_template_directory_uri(); ?>/images/hdg-01.png" alt=""></span>
        </div>
        <!--Heading Style 1 Wrap End-->
        <div class="row">
            <?php 
            // the query
            $args = 
            array(
              'post_type' => 'post',
              'category_name' =>'offer',
              'order'         =>'DESC',
              'posts_per_page'=> 3
             );


            $the_query = new WP_Query( $args ); ?>

            <?php if ( $the_query->have_posts() ) : ?>

            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 
            <div class="col-md-4 col-sm-6">
                <div class="get_started_services">
                    <div class="get_started_icon">
                        <i class="fa fa-paper-plane-o"></i>
                    </div>
                    <div class="get_icon_des">
                        <h5><?php the_title()?></h5>
                        <p><?php the_content()?></p>
                        <a href="<?php the_permalink()?>">View More <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
            <!-- end of the loop -->

            <!-- pagination here -->

            <?php wp_reset_postdata(); ?>

          <?php endif; ?>  
        </div>

    </div>
</section>