

<section>
    <div class="container">
        <!--Heading Style 1 Wrap Start-->
        <div class="ct_heading_1_wrap">
            <h3>Our Publications</h3>
            <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec <br/>tetuer adipis elit, aliquam eget nibh etlibura.</p>
            <span><img src="<?php echo get_template_directory_uri(); ?>/images/hdg-01.png" alt=""></span>
        </div>
        <!--Heading Style 1 Wrap End-->
        
        <!--Most Popular Course List Wrap Start-->
        <div class="most_popular_courses owl-carousel">

        <?php 
  // the query
        $args = 
        array(
          'post_type' => 'publication',
          'category_name' =>'publication',
          'order'         =>'ASC'
         );


        $the_query = new WP_Query( $args ); ?>

        <?php if ( $the_query->have_posts() ) : ?>

        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 


            <div class="item">
                <div class="ct_course_list_wrap">
                    <figure>
                        <img src="<?php the_post_thumbnail_url( 'medium' );?>"/>
                        <figcaption class="course_list_img_des">
                            <div class="ct_course_review">
                                <span><a href="<?php the_permalink();?>"><?php the_title(); ?></a></span>
                            </div>
                            <div class="ct_zoom_effect"></div>
                            <div class="ct_course_link">
                                <a href="<?php the_permalink();?>">View Detail</a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>

                <?php endwhile; ?>
                <!-- end of the loop -->

                <!-- pagination here -->

                <?php wp_reset_postdata(); ?>

              <?php endif; ?>
        </div>
        <!--Most Popular Course List Wrap End-->
        
    </div>
</section>