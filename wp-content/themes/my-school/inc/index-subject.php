        <section class="ct_courses_subject_bg">
        	<div class="container">
            	<!--Heading Style 1 Wrap Start-->
                <div class="ct_heading_1_wrap ct_white_hdg">
                	<h3>Cources By Subject</h3>
                    <span><img src="<?php echo get_template_directory_uri(); ?>/images/hdg-01.png" alt=""></span>
                </div>
                <!--Heading Style 1 Wrap End-->
                
                <!--Courses Subject List Wrap Start-->
                <div class="courses_subject_carousel owl-carousel">
                	<div class="item">
                        <div class="course_subject_wrap ct_bg_1">
                            <i class="fa fa-area-chart"></i>
                            <div class="course_subject_des">
                                <p><span>Industrial</span>Business Analysis</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_2">
                            <i class="fa fa-users"></i>
                            <div class="course_subject_des">
                                <p><span>Social Media</span>Management</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_3">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Business</span>Management</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_4">
                            <i class="fa fa-play-circle"></i>
                            <div class="course_subject_des">
                                <p><span>Movie</span>Production</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_5">
                            <i class="fa fa-television"></i>
                            <div class="course_subject_des">
                                <p><span>3D Arts</span>& Sciences</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Courses Subject List Wrap End-->
            </div>
        </section>