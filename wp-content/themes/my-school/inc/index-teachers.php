<section class="teacher_bg">
	<div class="container">
    	<!--Heading Style 1 Wrap Start-->
        <div class="ct_heading_1_wrap">
        	<h3>Our Teachers</h3>
            <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec <br/>tetuer adipis elit, aliquam eget nibh etlibura.</p>
            <span><img src="<?php echo get_template_directory_uri(); ?>/images/hdg-01.png" alt=""></span>
        </div>
        <!--Heading Style 1 Wrap End-->
        
        <!--Teacher List Wrap Start-->
        <div class="row">

            <?php 
            // the query
            $args = 
            array(
              'post_type' => 'teacher',
              'category_name' =>'teacher',
              'order'         =>'DESC',
              'posts_per_page'=> 4
             );


            $the_query = new WP_Query( $args ); ?>

            <?php if ( $the_query->have_posts() ) : ?>

            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 

        	<div class="col-md-3 col-sm-6">
            	<div class="ct_teacher_outer_wrap">
                	<figure>
                    	<img src="<?php the_post_thumbnail_url( 'medium' );?>"/>
                    </figure>
                    <div class="ct_teacher_wrap">
                    	<h5><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h5>
                        
                    </div>
                </div>
            </div>

        <?php endwhile; ?>
        <!-- end of the loop -->

        <!-- pagination here -->

        <?php wp_reset_postdata(); ?>

      <?php endif; ?>

        </div>
        <!--Teacher List Wrap End-->
        
    </div>
</section>