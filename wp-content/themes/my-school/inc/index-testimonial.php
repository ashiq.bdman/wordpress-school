        <section class="ct_facts_bg">
        	<div class="container">
            	<!--Heading Style 1 Wrap Start-->
                <div class="ct_heading_1_wrap ct_white_hdg">
                	<h3>Testimonials</h3>
                    <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec <br/>tetuer adipis elit, aliquam eget nibh etlibura.</p>
                    <span><img src="<?php echo get_template_directory_uri(); ?>/images/hdg-01.png" alt=""></span>
                </div>
                <!--Heading Style 1 Wrap End-->
                
                <!--Testimonial List Wrap Start-->
                <div class="testimonial_carousel owl-carousel">
                    <?php 
                  // the query
                        $args = 
                        array(
                          'post_type' => 'testimonial',
                          'category_name' => 'testimonial',
                          'order'         =>'ASC'
                         );



                        $the_query = new WP_Query( $args ); ?>

                        <?php if ( $the_query->have_posts() ) : ?>

                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 

                	<div class="item">
                    	<div class="testimonial_wrap">
                        	<figure>
                            	<img src="<?php  the_post_thumbnail_url('large'); ?>" alt="">
                            </figure>
                            <p><?php the_content();?></p>
						    <span><b><?php the_title(); ?></b></span>
                        </div>
                    </div>

                <?php endwhile; ?>
                <!-- end of the loop -->

                <!-- pagination here -->

                <?php wp_reset_postdata(); ?>

              <?php endif; ?>
                </div>
                <!--Testimonial List Wrap End-->
            </div>
        </section>