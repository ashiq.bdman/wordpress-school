<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package My_School
 */

get_header();
?>

    <!--Banner Wrap Start-->
	<?php get_template_part('inc/index', 'banner'); ?>
    <!--Banner Wrap End-->

    <!--Content Wrap Start-->
    <div class="ct_content_wrap">

        <!--Box section Wrap Start-->
        <?php get_template_part('inc/index', 'box'); ?>
        <!--Box section Wrap End-->

        <!-- End of # dic section -->

        <!--ALC Wrap Start-->
        <?php get_template_part('inc/index', 'alc'); ?>
        <!--ALC Wrap End-->

        <!--Get Started Wrap Start-->
        <section>
            <div class="container">
                <div class="get_started_outer_wrap">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="get_started_content_wrap ct_blog_detail_des_list">
                                <h3>Like what you’re learning GET STARTED TODAY!</h3>
                                <p>We are home to 1,500 students (aged 12 to 16) and 100 expert faculty and staff community representing over 40 different nations. We are proud of our international and multi-cultural ethos, the way our community collaborates to make a difference. Our world-renowned curriculum is built on the best.</p>
                                <p>Global and US standards.We are home to 1,500 students (aged 12 to 16) and 100 expert faculty and staff.Community representing over 40 different nations. We are proud of our international.</p>
                                <ul>
                                    <li>We are home to 1,500 students and 100 experts.</li>
                                    <li>Community representing over 40 of different nations.</li>
                                    <li>We are proud of our international.</li>
                                </ul>
                            </div>
                                                            <div style="margin-top: 10px">
                                    <a href="#" class="learn_btn" style="padding: 10px 20px;
    background: #f26727;
    color: #fff;">LEARN MORE</a>
                                </div>
                        </div>
                    
                        <div class="col-md-6">
                            <div class="get_started_video">
                                
                               <!--  <div class="get_video_icon">
                                    <a data-rel="prettyPhoto" href="https://www.youtube.com/watch?v=GYX3sn3Q_DQ"><i class="fa fa-play"></i></a>
                                    <span>Watch The Video</span>
                                </div> -->
 
<div class="video-wrapper">
    <video src="//clips.vorwaerts-gmbh.de/VfE_html5.mp4" poster="//s3-us-west-2.amazonaws.com/s.cdpn.io/3174/poster.png"></video>
</div>

                                
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
        </section>
        <!--Get Started Wrap End-->

        <!--PIS offering-->
        <?php get_template_part('inc/index', 'offering'); ?>
        <!--PIS offering Wrap End-->   

        
        <!--Our Events Wrap Start-->
        <?php get_template_part('inc/index', 'events'); ?>
        <!--Our Events Wrap End-->   


        <!--Testimonial Wrap Start-->
        <?php get_template_part('inc/index','testimonial'); ?>
        <!--Testimonial Wrap End-->

                <!--Latest News Wrap Start-->
        <?php get_template_part('inc/index','guest'); ?>
        <!--Latest News Wrap End-->

        <!--Latest News Wrap Start-->
        <?php get_template_part('inc/index','publications'); ?>
        <!--Latest News Wrap End-->


    </div>    
<script type="text/javascript">
    var videoPlayButton,
    videoWrapper = document.getElementsByClassName('video-wrapper')[0],
    video = document.getElementsByTagName('video')[0],
    videoMethods = {
        renderVideoPlayButton: function() {
            if (videoWrapper.contains(video)) {
                this.formatVideoPlayButton()
                video.classList.add('has-media-controls-hidden')
                videoPlayButton = document.getElementsByClassName('video-overlay-play-button')[0]
                videoPlayButton.addEventListener('click', this.hideVideoPlayButton)
            }
        },

        formatVideoPlayButton: function() {
            videoWrapper.insertAdjacentHTML('beforeend', '\
                <svg class="video-overlay-play-button" viewBox="0 0 200 200" alt="Play video">\
                    <circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#fff"/>\
                    <polygon points="70, 55 70, 145 145, 100" fill="#fff"/>\
                </svg>\
            ')
        },

        hideVideoPlayButton: function() {
            video.play()
            videoPlayButton.classList.add('is-hidden')
            video.classList.remove('has-media-controls-hidden')
            video.setAttribute('controls', 'controls')
        }
    }

videoMethods.renderVideoPlayButton()
</script>
<?php

get_footer();
