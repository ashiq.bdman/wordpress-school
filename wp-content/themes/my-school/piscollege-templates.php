 <?php
/*
Template Name: PIS college
*/

get_header();

?>

    
    <!--Banner Wrap Start-->
    <section class="sub_banner_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="sub_banner_hdg">
                        <h3>PIS College</h3>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ct_breadcrumb">
                        <ul>
                            <li><?php if (function_exists('wptricks_custom_breadcrumbs')){ wptricks_custom_breadcrumbs(); }?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Banner Wrap End-->

    <div class="row">

        <div class=" text-center" >
            <h2>Presidency International College</h2>
            
            <!-- <p><a class="btn btn-default">Click On Me!</a>
            <a class="btn btn-info">Tweet Me!</a></p> -->
        </div>

    </div>
    
    <!--Content Wrap Start-->
    <section>
        <div class="container">
            <div id="wrapper">

                <!-- Sidebar -->
                <div id="sidebar-wrapper">
                    <div class="nav-side-menu">
                        <div class="brand"> College Menu</div>
                        <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
                      
                            <div class="menu-list">
                      
                                <ul id="menu-content" class="menu-content collapse out">
                                    <!-- <li>
                                      <a href="#">
                                      <i class="fa fa-dashboard fa-lg"></i> Dashboard
                                      </a>
                                    </li> -->

                                    <li  data-toggle="collapse" data-target="#admission" class="collapsed active">
                                      <a href="#"><i class="fa fa-gift fa-lg"></i> Admission <span class="arrow"></span></a>
                                    </li>
                                    <ul class="sub-menu collapse" id="admission">
                                       <!--  <li class="active"><a href="#">CSS3 Animation</a></li> -->
                                        <li><a href="">Admission Home</a></li>
                                        <li><a href="#procedure">Procedure</a></li>
                                        <li><a href="#olevelsubjects">O'Level Subjects</a></li>
                                        <li><a href="#alevelsubjects">A' Level Subjects</a></li>
                                        <li><a href="#uniform">Uniform</a></li>
                                        <li><a href="">FAQ</a></li>
                                    </ul>


                                    <li data-toggle="collapse" data-target="#curriculam" class="collapsed">
                                      <a href="#"><i class="fa fa-globe fa-lg"></i> Curriculam <span class="arrow"></span></a>
                                    </li>  
                                    <ul class="sub-menu collapse" id="curriculam">
                                      <li><a href="">Curriculum Home</a></li>
                                      <li><a href="">Junior School </a></li>
                                      <li><a href="">Senior School </a></li>
                                      <li><a href="">Middle School </a></li>
                                    </ul>


                                    <li data-toggle="collapse" data-target="#facilities" class="collapsed">
                                      <a href="#"><i class="fa fa-globe fa-lg"></i> Facilities <span class="arrow"></span></a>
                                    </li>  
                                    <ul class="sub-menu collapse" id="facilities">
                                        <li><a href="">Facilities Home</a></li>
                                        <li><a href="">Science Lab</a> </li>
                                        <li><a href="">Computer Lab</a></li>
                                        <li><a href="#">Library</a></li>
                                        <li><a href="">Scholarship</a></li>
                                    </ul>


                                    <li data-toggle="collapse" data-target="#activities" class="collapsed">
                                      <a href="#"><i class="fa fa-globe fa-lg"></i> Activities <span class="arrow"></span></a>
                                    </li>  
                                    <ul class="sub-menu collapse" id="activities">
                                      <li><a href="">Curriculum Home </a></li>
                                      <li><a href="">Chess </a></li>
                                      <li><a href="">Debate</a></li>
                                      <li><a href="">Outing</a></li>
                                      <li><a href="">Performing Arts</a></li>
                                    </ul>


                                    <li data-toggle="collapse" data-target="#achievement" class="collapsed">
                                      <a href="#"><i class="fa fa-globe fa-lg"></i> Achievement  <span class="arrow"></span></a>
                                    </li>  
                                    <ul class="sub-menu collapse" id="achievement">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Academic</a></li>
                                        <li><a href="#">Sports</a></li>
                                        <li><a href="#">Trip to Abroad</a></li>
                                        <li><a href="#">Paper Cutting</a></li>
                                    </ul>

                                    <li data-toggle="collapse">
                                      <a href="#"><i class="fa fa-globe fa-lg"></i> Studentlife  <span class="arrow"></span></a>
                                    </li>

                                    <li data-toggle="collapse">
                                      <a href="#"><i class="fa fa-globe fa-lg"></i> Faculty  <span class="arrow"></span></a>
                                    </li> 

                                    <li data-toggle="collapse">
                                      <a href="#"><i class="fa fa-globe fa-lg"></i> Gallery  <span class="arrow"></span></a>
                                    </li> 
                                    
                                    <li data-toggle="collapse">
                                      <a href="#"><i class="fa fa-globe fa-lg"></i> Notice  <span class="arrow"></span></a>
                                    </li>  

                                </ul>
                         </div>
                    </div>
                </div>

                <!-- Page content -->
                <div class="row">
                    <div class="col-lg-12">
                                    <div id="page-content-wrapper">
                        <div class="page-content inset" data-spy="scroll" data-target="#spy">

                            <div class="row">
                                <div class="col-md-12 well">
                                    <legend id="procedure">Procedure</legend>
                                    <strong>Subjects of the Admission Test: </strong></span></p>

                                    <p>Admission test of the classes Nursery to VII will be held on the subjects of English, Mathematics and Bangla and those of classes VIII and upwards will be held as per students’ expected subjects/ group. For further query, please communicate with the SRO.</p>

                                    <p><span style="color:#000000"><strong>Timing of Admission Test:</strong></span></p>

                                    <ul>
                                        <li>First Admission Test will be held in May in every session.</li>
                                        <li>Interview of<strong> Playgroup</strong> will be held on first come first serve basis.</li>
                                        <li>Timing of Admission Test will be as follows:</li>
                                    </ul>

                                    <p>Nursery to KG : 1 hour 30 mins.</p>

                                    <p>Classes I to VII : 2 hours.</p>

                                    <p>VIII and upward : 2 hours (approximately)</p>

                                    <p><span style="color:#000000"><strong>Result of Admission Test:</strong></span></p>

                                    <p>The result of Admission Test along with the waiting list will be published on the notice board and in the website at 4:00 p.m. on the day of admission test.</p>

                                    <p><span style="color:#000000"><strong>Interview/Viva-voce:</strong></span></p>

                                    <ul>
                                        <li>If the applicant scores a satisfactory mark in the written test, he/she will be called for viva-voce according to a fixed schedule. Timing of interview will be different for different classes. The presence of the parents on the day of interview is a must.</li>
                                        <li>On the day of interview, the applicant should bring the following documents to the interview board:</li>
                                    </ul>

                                    <ol>
                                        <li>Original copy of school transcript of previous class for the whole academic session.</li>
                                        <li>Original copy of testimonial/school leaving certificate/recommendation letter from the Principal/Head of the previous school.</li>
                                        <li>Original copy of birth certificate.</li>
                                    </ol>

                                    <p><span style="color:#000000"><strong>Admission: </strong></span></p>

                                    <ul>
                                        <li>Admission will be held according to a fixed schedule/ date.</li>
                                        <li>For admission fee and other fee, the admission seekers have to communicate with the Accounts Office.</li>
                                        <li>At the time of admission, a notice will be given to the new students mentioning the date of starting the class, school timing, date of collecting books stationeries from the school.</li>
                                    </ul>

                                    <p></p>

                                    <p><strong>[N.B.: The authority preserves right to change any of the above rules if necessary.]</strong></p>
                                </div>

                                <div class="col-md-12 well">
                                    <legend id="olevelsubjects">O Level Subjects</legend>

                                    <table border="1" cellpadding="5" cellspacing="0" style="border-collapse:collapse; width:100%" class="table table-responsive table-stripe">
                                        <tbody>
                                            <tr>
                                                <td colspan="4">
                                                <p style="text-align:center"><span style="color:#808000"><strong>Students of Class IX appear at the O’ Level Exams in</strong></span><br>
                                                <span style="color:#808000"><strong><em>May / June Session</em></strong></span></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong><em>SL</em></strong></td>
                                                <td><strong><em>Subject</em></strong></td>
                                                <td><strong><em>Code</em></strong></td>
                                                <td><strong><em>Duration</em></strong></td>
                                            </tr>
                                            <tr>
                                                <td>01</td>
                                                <td>Bangladesh Studies</td>
                                                <td>7094/12<br>
                                                7094/22</td>
                                                <td>1 h 30 m<br>
                                                1 h 30 m</td>
                                            </tr>
                                            <tr>
                                                <td>02</td>
                                                <td>Bengali</td>
                                                <td>3204/1<br>
                                                3204/2</td>
                                                <td>2 h<br>
                                                1 h 30 m</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                <p style="text-align:center"><span style="color:#333399"><strong>Students of Class X appear at the O’ Level Exams in</strong></span></p>

                                                <p style="text-align:center"><span style="color:#333399"><strong><em>May / June Session</em></strong></span></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong><em>Sl</em></strong></td>
                                                <td><strong><em>Subjects</em></strong></td>
                                                <td>
                                                <p><strong><em>Code</em></strong></p>
                                                </td>
                                                <td>
                                                <p><strong><em>Duration</em></strong></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>01</p>
                                                </td>
                                                <td>
                                                <p>English Language</p>
                                                </td>
                                                <td>
                                                <p>2058/12<br>
                                                2058/22</p>
                                                </td>
                                                <td>
                                                <p>2 h<br>
                                                1 h 30 m</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>02</p>
                                                </td>
                                                <td>
                                                <p>literature in English<br>
                                                literature in English</p>
                                                </td>
                                                <td>
                                                <p>2010/12<br>
                                                2010/22</p>
                                                </td>
                                                <td>
                                                <p>1 h 30 m<br>
                                                1 h 45 m</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>03</p>
                                                </td>
                                                <td>
                                                <p>Additional Mathematics</p>
                                                </td>
                                                <td>
                                                <p>4037/12<br>
                                                4037/22</p>
                                                </td>
                                                <td>
                                                <p>2 h<br>
                                                2 h</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>04</p>
                                                </td>
                                                <td>
                                                <p>Physics</p>
                                                </td>
                                                <td>
                                                <p>5054/12<br>
                                                5054/22<br>
                                                5054/32<br>
                                                5054/42</p>
                                                </td>
                                                <td>
                                                <p>1 h<br>
                                                1 h 45 m<br>
                                                2 h<br>
                                                1 h</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>05</p>
                                                </td>
                                                <td>
                                                <p>Chemistry</p>
                                                </td>
                                                <td>
                                                <p>5070/12<br>
                                                5070/22<br>
                                                5070/32<br>
                                                5070/42</p>
                                                </td>
                                                <td>
                                                <p>1 h<br>
                                                1 h 30m<br>
                                                1h 30m<br>
                                                1 h</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>06</p>
                                                </td>
                                                <td>
                                                <p>Biology</p>
                                                </td>
                                                <td>
                                                <p>5090/12<br>
                                                5090/22<br>
                                                5090/32<br>
                                                5090/62</p>
                                                </td>
                                                <td>
                                                <p>1 h<br>
                                                1 h 45 m<br>
                                                1 h 15 m<br>
                                                1 h</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>07</p>
                                                </td>
                                                <td>
                                                <p>Business Studies</p>
                                                </td>
                                                <td>
                                                <p>7115/11<br>
                                                7115/12</p>
                                                </td>
                                                <td>
                                                <p>1 h 45 m<br>
                                                1 h 45 m</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>08</p>
                                                </td>
                                                <td>
                                                <p>Principles of Accounts</p>
                                                </td>
                                                <td>
                                                <p>7110/12<br>
                                                7110/22</p>
                                                </td>
                                                <td>
                                                <p>1 h<br>
                                                2 h</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>09</p>
                                                </td>
                                                <td>
                                                <p>Commerce</p>
                                                </td>
                                                <td>
                                                <p>7100/12<br>
                                                7100/22</p>
                                                </td>
                                                <td>
                                                <p>1 h<br>
                                                2 h</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>10</p>
                                                </td>
                                                <td> Economics</td>
                                                <td>7100/12<br>
                                                7100/22</td>
                                                <td>1 h<br>
                                                2 h</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>11</p>
                                                </td>
                                                <td>
                                                <p>Computer Science</p>
                                                </td>
                                                <td>
                                                <p>2210/12<br>
                                                2210/32</p>
                                                </td>
                                                <td>
                                                <p>1 h 45 m<br>
                                                1 h 45 m</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>12</p>
                                                </td>
                                                <td>
                                                <p>Mathematics – D</p>
                                                </td>
                                                <td>
                                                <p>4024/12<br>
                                                4024/22</p>
                                                </td>
                                                <td>
                                                <p>1 h 30 m<br>
                                                1 h 45 m</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <p>13</p>
                                                </td>
                                                <td>
                                                <p>Islamiyat</p>
                                                </td>
                                                <td>
                                                <p>2058/12</p>

                                                <p>2058/22</p>
                                                </td>
                                                <td>
                                                <p>1h 30 m</p>

                                                <p>1h 45 m</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div>
                                <div class="col-md-12 well">
                                    <legend id="alevelsubjects">A Level Subjects</legend>
                                        <table border="1" cellpadding="1" cellspacing="1" style="width:100%" class="table table-responsive table-stripe">
                                            <tbody>
                                                <tr>
                                                    <td colspan="4">
                                                    <p style="text-align:center">Subjects offered in AS Level</p>

                                                    <p style="text-align:center"><strong><em>May / June Session</em></strong></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span style="color:#800000"><strong><em>Sl</em></strong></span></td>
                                                    <td><span style="color:#800000"><strong><em>Subjects</em></strong></span></td>
                                                    <td><span style="color:#800000"><strong><em>Code</em></strong></span></td>
                                                    <td><span style="color:#800000"><strong><em>Duration</em></strong></span></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">01</td>
                                                    <td style="text-align:center">Accounting</td>
                                                    <td style="text-align:center">9706/01<br>
                                                    9706/02</td>
                                                    <td style="text-align:center">1 h<br>
                                                    1 h 30 m</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">02</td>
                                                    <td style="text-align:center">Biology</td>
                                                    <td style="text-align:center">9700/01<br>
                                                    9700/02<br>
                                                    9700/03</td>
                                                    <td style="text-align:center">1 h<br>
                                                    1 h 15<br>
                                                    2 h</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">03</td>
                                                    <td style="text-align:center">Business Studies</td>
                                                    <td style="text-align:center">9707/01<br>
                                                    9707/02</td>
                                                    <td style="text-align:center">1 h 15 m<br>
                                                    1 h 30 m</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">04</td>
                                                    <td style="text-align:center">Chemistry</td>
                                                    <td style="text-align:center">9701/01<br>
                                                    9701/02<br>
                                                    9701/03</td>
                                                    <td style="text-align:center">1 h<br>
                                                    1 h 30 m<br>
                                                    1 h 15 m</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">05</td>
                                                    <td style="text-align:center">Computer Science</td>
                                                    <td style="text-align:center">7010/01<br>
                                                    7010/02</td>
                                                    <td style="text-align:center">1 h 45 m<br>
                                                    …………</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">06</td>
                                                    <td style="text-align:center">Economics</td>
                                                    <td style="text-align:center">9708/01<br>
                                                    9708/02</td>
                                                    <td style="text-align:center">1 h<br>
                                                    1 h 30 m</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">07</td>
                                                    <td style="text-align:center">English Language (AS)<br>
                                                    English Language (AS)<br>
                                                    Literature in English (AS)<br>
                                                    Literature in English (AS)</td>
                                                    <td style="text-align:center">9093/12<br>
                                                    9093/22<br>
                                                    9695/32<br>
                                                    9695/42</td>
                                                    <td style="text-align:center">2 h
                                                    <p></p>

                                                    <p>2 h</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">08</td>
                                                    <td style="text-align:center">Literature in English<br>
                                                    Literature in English<br>
                                                    Literature in English</td>
                                                    <td style="text-align:center">9695/52<br>
                                                    9695/62<br>
                                                    9695/72</td>
                                                    <td style="text-align:center">9695/52<br>
                                                    9695/62<br>
                                                    9695/72</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">09</td>
                                                    <td style="text-align:center">Mathematics</td>
                                                    <td style="text-align:center">9709/01<br>
                                                    9709/04</td>
                                                    <td style="text-align:center">1 h 45 m<br>
                                                    1 h 15 m</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center">10</td>
                                                    <td style="text-align:center">Physics</td>
                                                    <td style="text-align:center">9702/01<br>
                                                    9702/02<br>
                                                    9702/03</td>
                                                    <td style="text-align:center">1 h<br>
                                                    1 h<br>
                                                    2 h</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </div>
                                <div class="col-md-12 well">
                                    <legend id="uniform">Uniform</legend>
                                    <h4>Uniform (Boys’)</h4>

                                    <p><span><strong>Play group – KG</strong></span></p>

                                    <p>White Navy blue half-sleeved check shirt, navy blue half pant, white socks and white keds.</p>

                                    <p><strong><span>Class I – IV</span></strong></p>

                                    <p>White and Navy blue half-sleeved check shirt, navy blue trousers, white socks and black shoes with lace white keds (for Physical Education class only).</p>

                                    <p><strong>Classes V and Above</strong></p>

                                    <p>White Navy blue half-sleeved check shirt, full pant, navy blue tie, white socks, black shoes with lace white keds (for Physical Education class only).</p>

                                    <h4>Uniform (Girls’)</h4>

                                    <p><strong>Play group – KG</strong></p>

                                    <p>White and Navy blue half-sleeved check frock with navy blue belt, white socks and white keds.</p>

                                    <p><strong>Class I II</strong></p>

                                    <p>White and Navy blue half-sleeved check frock with navy blue belt, white socks, black belco shoes white keds (for Physical Education class only).</p>

                                    <p><strong>Class III IV</strong></p>

                                    <p>White and Navy blue half-sleeved check frock with navy blue belt, white salwar, white scarf, white socks, black velcro shoes white keds (for Physical Education class only).</p>

                                    <p><strong>Class V – VII</strong></p>

                                    <p>White and Navy blue full-sleeved check frock with navy blue belt, white ‘V’ shaped orna, white salwar, white scarf, white socks, black Velcro shoes.</p>

                                    <p><strong>Class VIII above</strong></p>

                                    <p>White and Navy blue half-sleeved check kameez with navy blue belt, white ‘V’ shaped orna, white salwar, white scarf, white socks, black velcro shoes. A white apron to be worn above.</p>

                                    <p><strong>Winter Clothing</strong></p>

                                    <p>Students must wear navy blue full-sleeved sweaters.</p>

                                    <p><strong>General information </strong></p>
                                    <ul style="list-style-type: circle;">
                                        <li>Students must keep two sets of uniforms and socks for a week. They have to put on a neat clean ironed uniform.</li>
                                        <li>Parents are expected to keep a set of casual dress in their child’s bag (for Play group, Nursery and K.G in particular).</li>
                                        <li>Students must have clean and short nails.</li>
                                        <li>Boys should have trimmed hair and Girls’ hair must be tied properly with white or black hair band.</li>
                                        <li><strong>Fancy shoes</strong> or any other ornamental items are not allowed.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>   

                </div>

            </div>
        </div>
    </section>
    <!--Content Wrap End-->

<?php get_footer();?>    