<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package My_School
 */

get_header();
?>
<!--Banner Wrap Start-->
<section class="sub_banner_wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="sub_banner_hdg">
                    <h3>Publication Details</h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ct_breadcrumb">
                    <ul>
                        <li><?php if (function_exists('wptricks_custom_breadcrumbs')){ wptricks_custom_breadcrumbs(); }?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Banner Wrap End-->
<div class="ct_content_wrap">
    <section class="ct_blog_outer_wrap">
        <div class="container">
            <div class="row">

             <?php
                while ( have_posts() ) :the_post();

             ?>
             <!--Blog Detail Wrap Start-->
                <div class="col-md-8">
                    <div class="ct_blog_detail_outer_wrap">
                        <div class="ct_blog_detail_top">
                            <h4><?php the_title(); ?></h4>
                        </div>
                        <div class="ct_blog_detail_des">
                            <figure>
                                <img src="<?php the_post_thumbnail_url( 'medium' );?>"/>
                            </figure>
                            <div class="ct_course_detail_wrap">
                                <h5>Detail Information</h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="ct_blog_detail_des_list">
                                            <p><?php echo wpautop( the_content());?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <?php
                                //Get the images ids from the post_metadata
                                $images = acf_photo_gallery('gallery', $post->ID);
                                //Check if return array has anything in it
                                if( count($images) ):
                                    //Cool, we got some data so now let's loop over it
                                    foreach($images as $image):
                                        $id = $image['id']; // The attachment id of the media
                                        $title = $image['title']; //The title
                                        $caption= $image['caption']; //The caption
                                        $full_image_url= $image['full_image_url']; //Full size image url
                                        $full_image_url = acf_photo_gallery_resize_image($full_image_url, 262, 160); //Resized size to 262px width by 160px height image url
                                        $thumbnail_image_url= $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                                        $url= $image['url']; //Goto any link when clicked
                                        $target= $image['target']; //Open normal or new tab
                                        $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
                                        $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
                            ?>
                            <div class="col-xs-6 col-md-3">
                                <div class="thumbnail">
                                    <?php if( !empty($url) ){ ?><a href="<?php echo $url; ?>" <?php echo ($target == 'true' )? 'target="_blank"': ''; ?>><?php } ?>
                                        <a href="<?php echo $full_image_url; ?>"><img src="<?php echo $full_image_url; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>"></a>
                                    <?php if( !empty($url) ){ ?></a><?php } ?>
                                </div>
                            </div>
                            <?php endforeach; endif; ?>
                            </div>                          
                        </div>

                        <div class="pre_next">
                            <?php previous_post_link( '%link','Previous' ) ?>

                            
                            <?php next_post_link( '%link','Next' ) ?>

                        </div>

                    </div>
                </div>
                <!--Blog Detail Wrap End-->
                <!--Aside Bar Wrap Start-->
                <div class="col-md-4">
                    <aside class="gt_aside_outer_wrap">
      
                        <!--Recent News Wrap Start-->
                        <div class="gt_aside_post_wrap gt_detail_hdg aside_margin_bottom">
                            <?php dynamic_sidebar( 'sidebar-1' ); ?>
                        </div>
                        <!--Recent News Wrap Start-->
                        
                        <!--Recent News Wrap Start-->
                        <div class="ct_popular_course gt_detail_hdg aside_margin_bottom">
                            <?php dynamic_sidebar( 'sidebar-2' ); ?>
                        </div>
                        <!--Recent News Wrap Start-->
                        
                    </aside>
                </div>
                <!--Aside Bar Wrap End-->


            </div>
        </div>
    </section>

</div>

<?php
endwhile; // End of the loop.
?>



<?php
get_footer();
