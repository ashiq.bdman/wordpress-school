<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package My_School
 */

get_header();
?>
<!--Banner Wrap Start-->
<section class="sub_banner_wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="sub_banner_hdg">
                    <h3>Course Detail</h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ct_breadcrumb">
                    <ul>
                        <li><?php if (function_exists('wptricks_custom_breadcrumbs')){ wptricks_custom_breadcrumbs(); }?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Banner Wrap End-->

    <!--Banner Wrap End-->
    <div class="ct_content_wrap">
        <section class="ct_blog_outer_wrap">
            <div class="container">
                <div class="row">
                	
				<!--Blog Detail Wrap Start-->
				    <div class="col-md-8">
				        <div class="ct_blog_detail_outer_wrap">

									<?php
									while ( have_posts() ) :
										the_post();


										get_template_part( 'template-parts/content', get_post_type() );



										// If comments are open or we have at least one comment, load up the comment template.
										if ( comments_open() || get_comments_number() ) :
											comments_template();
										endif;

									endwhile; // End of the loop.
									?>
				            <div class="pre_next">
				                <?php previous_post_link( '%link','Previous' ) ?>

				                
				                <?php next_post_link( '%link','Next' ) ?>

				            </div>

				        </div>
				    </div>
				<!--Blog Detail Wrap End-->
                                <!--Aside Bar Wrap Start-->
                <div class="col-md-4">
                    <aside class="gt_aside_outer_wrap">
      
                        <!--Recent News Wrap Start-->
                        <div class="gt_aside_post_wrap gt_detail_hdg aside_margin_bottom">
                            <?php get_sidebar(); ?>
                        </div>
                        <!--Recent News Wrap Start-->
                        
                    </aside>
                </div>
                <!--Aside Bar Wrap End-->
                </div>
            </div>
        </section>

    </div>

<?php

get_footer();
