    
<!--ALC Wrap Start-->
<div class="row">
	<div class="col-md-6">
    	<div class="ct_main_event_wrap">
            <h4><?php the_title(); ?></h4>
            <ul class="event_location_list">
                <li><span><a href="<?php the_permalink();?>"  style="color:#fff">Read More <i class="fa fa-arrow-right"></i></a></span></li>
            </ul>
        </div>
        <div class="popular_course_des">
           
        </div>
    </div>      
    <div class="col-md-6">
        <div class="popular_course_des">
            <p><?php $content = get_the_content(); echo mb_strimwidth($content, 0, 300, '...');?></p>
             <p><span><a href="<?php the_permalink();?>"  style="color:#fff">Read More <i class="fa fa-arrow-right"></i></a></span></p>
        </div>
    </div>           
</div>
<!--AlC Wrap End-->
        