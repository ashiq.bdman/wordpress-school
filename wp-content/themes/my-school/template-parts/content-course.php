<div class="col-md-4 col-sm-6">
    <div class="ct_course_list_wrap">
        <figure>
            <img src="<?php the_post_thumbnail_url( 'medium' );?>"/>
        </figure>
        <div class="popular_course_des">
            <h5><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h5>
            <div class="ct_course_meta">
                <div class="course_author">
                    <i class="fa fa-user"></i><a href="#"><?php the_author();?></a>
                </div>
                <ul>
                    <li><i class="fa fa-calendar"></i><a href="#"><?php the_time(' F jS, Y') ?> </a></li>
                </ul>

            </div>
        </div>
    </div>
</div>



