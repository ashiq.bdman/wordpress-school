    
<!--Event List Wrap Start-->

	<div class="col-md-6">
    	<div class="ct_main_event_wrap">
            <h4><?php the_title(); ?></h4>
            <ul class="event_location_list">
                <li><span><a href="<?php the_permalink();?>"  style="color:#fff">Read More <i class="fa fa-arrow-right"></i></a></span></li>
            </ul>
        </div>
        <div class="popular_course_des">
            <h5><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h5>
        </div>
    </div>               

<!--Event List Wrap End-->
        