<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package My_School
 */

?>

<div class="ct_blog_detail_des">
    <div class="ct_course_detail_wrap">
        <div class="row">
            <div class="col-md-3">
                <img src="<?php the_post_thumbnail_url( 'medium' );?>"/>
            </div>
            <div class="col-md-9">
                <div class="ct_blog_detail_des_list">
                	<h4><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h4>
                    <p><?php echo wpautop( the_content());?></p>
                </div>
            </div>
        </div>
    </div>
</div>


