<?php

$teacher_name   = get_field('teacher_name');
$subject        = get_field('subject');
$class          = get_field('class');
$degree         = get_field('degree');


?>

<div class="col-md-3 col-sm-6">
    <div class="ct_course_list_wrap">
        <figure>
            <img src="<?php the_post_thumbnail_url( 'medium' );?>"/>
        </figure>
        <div class="popular_course_des">
            <h5><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h5>
            <div class="ct_course_meta">
                <div class="course_author">
                    <i class="fa fa-user"></i><a href="#"><?php echo $degree; ?></a>
                </div>
            </div>
        </div>
    </div>
</div>



